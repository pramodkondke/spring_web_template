package com.app.template.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author pramodk
 *
 */
@Controller
@RequestMapping("/template")
public class TemplateController {
	
	public TemplateController() {
		System.out.println("in constructor of : "+ this.getClass());
	}

	@RequestMapping(value="/show", method = RequestMethod.GET)
	public String showTemplatePage(ModelMap modelObj) {	
		modelObj.addAttribute("welcomeTitle", "Spring Web MVC Template");
		modelObj.addAttribute("messageObj", "This Page Demonstrates How To Use Spring MVC !");
		return "template";
	}
}
