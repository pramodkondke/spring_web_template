# Spring MVC Web App Template
## Introduction :
- This template gives a basic understanding of deployment descriptor(web.xml) file.
- Dispatcher Servlet
- Web Application Context Vs Root Application Context
- mvc annotation basics
- contextConfigLocation and ContextLoaderListener class
- welcome file list
- Request Handlermapping
